package com.motorola.moto4u.eldoradotraining.activity

import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import com.motorola.moto4u.eldoradotraining.R
import com.motorola.moto4u.eldoradotraining.adapter.ContactAdapter
import com.motorola.moto4u.eldoradotraining.model.Contact

class MainActivity : AppCompatActivity() {

    private lateinit var contactRecyclerView: RecyclerView
    private lateinit var viewAdapter: ContactAdapter
    private lateinit var viewManager: RecyclerView.LayoutManager
    private lateinit var mDataSet : Array<Contact>
    private lateinit var mEditText : EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mEditText = findViewById(R.id.add_contact_edit_text)

        mDataSet = arrayOf<Contact>()
        mDataSet += Contact("Josh")
        mDataSet += Contact("Fabio")
        mDataSet += Contact("Douglas")

        viewManager = LinearLayoutManager(this)
        viewAdapter = ContactAdapter(mDataSet)

        contactRecyclerView = findViewById<RecyclerView>(R.id.contacts_recycler_view).apply {
            layoutManager = viewManager
            adapter = viewAdapter
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    fun addContact(view : View) {
        mDataSet += Contact(mEditText.text.toString())
        mEditText.text.clear()
        viewAdapter.setItems(mDataSet)
    }
}

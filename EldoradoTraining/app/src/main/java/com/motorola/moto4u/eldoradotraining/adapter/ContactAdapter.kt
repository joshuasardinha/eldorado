package com.motorola.moto4u.eldoradotraining.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.view.View
import com.motorola.moto4u.eldoradotraining.R
import com.motorola.moto4u.eldoradotraining.model.Contact
import kotlinx.android.synthetic.main.item_contact.view.*

class ContactAdapter(private var myDataSet: Array<Contact>) :
        RecyclerView.Adapter<ContactAdapter.MyViewHolder>() {

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder.
    // Each data item is just a string in this case that is shown in a TextView.
    class MyViewHolder(view : View) : RecyclerView.ViewHolder(view) {
        val contactName = view.contact_item_name
        val contactLetter = view.contact_item_letter
    }


    // Create new views (invoked by the layout manager)
    override fun onCreateViewHolder(parent: ViewGroup,
                                    viewType: Int): ContactAdapter.MyViewHolder {
        // create a new view
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.item_contact, parent, false) // as ConstraintLayout
        // set the view's size, margins, paddings and layout parameters
        return MyViewHolder(view)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        val contact : Contact = myDataSet[position]
        holder.contactName?.text = contact.getName()
        holder.contactLetter?.text = contact.getName().subSequence(0,1)
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = myDataSet.size

    public fun setItems(newArray : Array<Contact>) {
        myDataSet = newArray
        notifyDataSetChanged()
    }
}
package com.eldorado.joshuasardinha.weatherforecastjoshua.util

import android.arch.persistence.room.TypeConverter
import com.eldorado.joshuasardinha.weatherforecastjoshua.model.Coordinates
import org.joda.time.DateTime

class Converters {

    @TypeConverter
    fun fromCoordinates(coord: Coordinates): String {
        return ""
    }

    @TypeConverter
    fun toCoordinates(value: String): Coordinates {
        return Coordinates(1.0,1.0)
    }

    @TypeConverter
    fun fromDateTime(dateTime : DateTime) : String {
        return dateTime.toString()
    }

    @TypeConverter
    fun toDateTime(dateTime : String) : DateTime {
        return DateTime(dateTime)
    }
}

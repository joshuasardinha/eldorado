package com.eldorado.joshuasardinha.weatherforecastjoshua.database.city

import android.app.Application
import android.arch.lifecycle.LiveData
import android.os.AsyncTask
import com.eldorado.joshuasardinha.weatherforecastjoshua.database.RoomDB
import com.eldorado.joshuasardinha.weatherforecastjoshua.model.City
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

class CityRepository(application: Application) {
    private val db: RoomDB = RoomDB.getDatabase(application)!!
    private val mDao: CityDao = db.cityDao()
    private var mCities: LiveData<List<City>> = mDao.getAllCities()

    fun insert(city: City) {
        doAsync {
            mDao.insert(city)
        }
    }

    fun getAllCities(): LiveData<List<City>> {
        return mCities
    }

    fun delete(city: City) {
        doAsync {
            mDao.delete(city)
        }
    }

    fun getCityByName(cityName : String) : City {
        return GetCityByNameAsyncTask(mDao).execute(cityName).get()
    }

    private class GetCityByNameAsyncTask(private val mAsyncTaskDao: CityDao) : AsyncTask<String, Void, City>() {
        override fun doInBackground(vararg params: String): City {
            return mAsyncTaskDao.getCityByName(params[0])
        }

        override fun onPostExecute(result: City?) {
            super.onPostExecute(result)
        }
    }
}
package com.eldorado.joshuasardinha.weatherforecastjoshua.service.listener


import com.eldorado.joshuasardinha.weatherforecastjoshua.model.Model

interface WeatherServiceListener {

    fun onSuccess(weather: Model?, code: Int)

    fun onFailure()
}
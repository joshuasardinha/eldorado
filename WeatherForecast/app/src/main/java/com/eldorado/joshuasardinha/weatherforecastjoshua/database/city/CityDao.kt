package com.eldorado.joshuasardinha.weatherforecastjoshua.database.city

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.persistence.room.*
import android.arch.persistence.room.Dao
import com.eldorado.joshuasardinha.weatherforecastjoshua.model.City
import com.eldorado.joshuasardinha.weatherforecastjoshua.model.Model

@Dao
interface CityDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(city: City)

    @Query("SELECT * FROM city_table")
    fun getAllCities(): LiveData<List<City>>

    @Delete
    fun delete(city: City)

    @Query("SELECT * FROM city_table WHERE name = :cityName")
    fun getCityByName(cityName : String) : City
}
package com.eldorado.joshuasardinha.weatherforecastjoshua.model

data class Wind(val speed: Float, val deg: Float)
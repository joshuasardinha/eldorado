package com.eldorado.joshuasardinha.weatherforecastjoshua.view.weather

import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import com.eldorado.joshuasardinha.weatherforecastjoshua.R
import com.eldorado.joshuasardinha.weatherforecastjoshua.database.city.CityRepository
import com.eldorado.joshuasardinha.weatherforecastjoshua.model.City
import com.eldorado.joshuasardinha.weatherforecastjoshua.model.CityWeather
import com.eldorado.joshuasardinha.weatherforecastjoshua.viewModel.WeatherViewModel

import kotlinx.android.synthetic.main.activity_city_weather.*
import kotlinx.android.synthetic.main.content_city_weather.*
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import org.joda.time.format.DateTimeFormatter
import org.joda.time.tz.DateTimeZoneBuilder
import java.util.*


class CityWeatherActivity : AppCompatActivity() {

    private lateinit var mWeatherViewModel : WeatherViewModel
    private val mContext : Context = this

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_city_weather)

        // Get current City data
        var cityName: String = intent.extras!!.getString("CityName")!!
        val cityRepository = CityRepository(application)
        val city : City = cityRepository.getCityByName(cityName!!)

        // ViewModel setup
        mWeatherViewModel = ViewModelProviders.of(this).get(WeatherViewModel::class.java)

        // Get this city weathers list
        val weathersList : List<CityWeather> = mWeatherViewModel.getWeathersFromCity(cityName)

        // Sets up header with city name and current date
        val cityText : String = cityName + ", " + city.country
        weatherCityName.text = cityText
        weatherDate.text = city.lastUpdatedDate.toString("dd/MM/yyyy")

        // Sets up current temperature and main conditions
        val tempText : String = ((weathersList[0].temp_max + weathersList[0].temp_min).toInt()/2 - 273).toString() + "°"
        weatherTemp.text = tempText
        weatherCond.text = (weathersList[0].main)

        // Sets up background according to current time in the place
        val formatter : DateTimeFormatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")
        val date : DateTime = formatter.parseDateTime(weathersList[0].dt_txt)
        val hourOfDay : Int = date.hourOfDay().get()
        if (hourOfDay in 4..6 || hourOfDay in 17..19) {
            cityWeatherBackground.setImageResource(R.drawable.mid)
        } else if (hourOfDay in 6..17) {
            cityWeatherBackground.setImageResource(R.drawable.day)
        } else {
            cityWeatherBackground.setImageResource(R.drawable.night)
        }

        // Initializes recyclerView with weather data
        val recyclerView = weathersRecyclerView
        recyclerView.apply {
            adapter = WeatherAdapter(mContext, weathersList)
            layoutManager = LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false)
        }
    }

}
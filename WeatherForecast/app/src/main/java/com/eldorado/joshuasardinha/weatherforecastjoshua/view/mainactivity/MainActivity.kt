package com.eldorado.joshuasardinha.weatherforecastjoshua.view.mainactivity

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.helper.ItemTouchHelper
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.eldorado.joshuasardinha.weatherforecastjoshua.R
import com.eldorado.joshuasardinha.weatherforecastjoshua.model.City
import com.eldorado.joshuasardinha.weatherforecastjoshua.model.Model
import com.eldorado.joshuasardinha.weatherforecastjoshua.service.WeatherManager
import com.eldorado.joshuasardinha.weatherforecastjoshua.service.listener.WeatherServiceListener
import com.eldorado.joshuasardinha.weatherforecastjoshua.viewModel.CityViewModel
import com.eldorado.joshuasardinha.weatherforecastjoshua.viewModel.WeatherViewModel

import kotlinx.android.synthetic.main.content_main.*
import org.joda.time.DateTime
import java.text.Normalizer


class MainActivity : AppCompatActivity() {

    private val mContext: Context = this
    private lateinit var mCityViewModel: CityViewModel
    private lateinit var mWeatherViewModel : WeatherViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mCityViewModel = ViewModelProviders.of(this).get(CityViewModel::class.java)
        mWeatherViewModel = ViewModelProviders.of(this).get(WeatherViewModel::class.java)
        val weatherManager = WeatherManager(this)

        insertCityButton.setOnClickListener { view ->
            progressBar.visibility = View.VISIBLE

            var insertedCityName : String = insertCityEditText.text.toString()
            insertCityEditText.text?.clear()
            // Clear accents punctuation
            insertedCityName = Normalizer.normalize(insertedCityName, Normalizer.Form.NFD)
            insertedCityName = insertedCityName.replace("[\\p{InCombiningDiacriticalMarks}]", "")

            weatherManager.getCityWeather(object : WeatherServiceListener {
                override fun onSuccess(weather: Model?, code: Int) {
                    progressBar.visibility = View.GONE
                    if (code == 404) {
                        Toast.makeText(mContext, "City does not exist!", Toast.LENGTH_LONG).show()
                    } else if (code == 200) {
                        if (weather != null && weather.list.isNotEmpty()) {
                            val city : City = weather.city
                            city.lastUpdatedDate = DateTime.now()
                            mCityViewModel.insert(city)
                            mWeatherViewModel.updateWeathersFromCity(city, weather)
                        } else {
                            Toast.makeText(mContext, "Server did not post weathers list, try again.", Toast.LENGTH_LONG).show()
                        }
                    }
                }

                override fun onFailure() {
                    progressBar.visibility = View.GONE
                }
            }, insertedCityName)
        }

        val recyclerView: RecyclerView = cityRecyclerView
        val adapter = CityAdapter(this, mWeatherViewModel)
        recyclerView.apply {
            this.adapter = adapter
            this.layoutManager = LinearLayoutManager(mContext, LinearLayoutManager.HORIZONTAL, false)
        }

        mCityViewModel.getAllCities().observe(this, Observer { cities ->
            adapter.setCities(cities)
        })

        val helper = ItemTouchHelper(object : ItemTouchHelper.SimpleCallback(0,
                ItemTouchHelper.UP or ItemTouchHelper.DOWN) {
            override fun onMove(recyclerView: RecyclerView,
                                viewHolder: RecyclerView.ViewHolder,
                                target: RecyclerView.ViewHolder): Boolean {
                return false
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val position = viewHolder.adapterPosition
                val myCity = adapter.getCityAtPosition(position)
                Toast.makeText(this@MainActivity, "Deleting " + myCity.name, Toast.LENGTH_SHORT).show()

                mCityViewModel.delete(myCity)
            }
        } )

        helper.attachToRecyclerView(recyclerView)

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }
}

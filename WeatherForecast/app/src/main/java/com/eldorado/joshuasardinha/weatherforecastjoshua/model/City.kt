package com.eldorado.joshuasardinha.weatherforecastjoshua.model

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.Index
import android.arch.persistence.room.PrimaryKey
import android.support.annotation.NonNull
import org.joda.time.DateTime
import java.util.*

@Entity(tableName = "city_table")
class City{
    @NonNull
    @PrimaryKey
    @ColumnInfo(name = "id")
    var id: Int = 0

    @ColumnInfo(name="name")
    var name: String = ""

    @ColumnInfo(name="coordinates")
    var coord: Coordinates = Coordinates(0.0, 0.0)

    @ColumnInfo(name="country")
    var country: String = ""

    @ColumnInfo(name = "lastUpdatedDate")
    var lastUpdatedDate : DateTime = DateTime()

    override fun equals(other: Any?): Boolean {
        if (other == null)
            return false // null check
        if (javaClass != other.javaClass)
            return false // type check

        val mOther = other as City
        return id == mOther.id
                && name == mOther.name
    }

    override fun hashCode(): Int {
        return 17*id.hashCode()*name.hashCode() + country.hashCode()
    }
}
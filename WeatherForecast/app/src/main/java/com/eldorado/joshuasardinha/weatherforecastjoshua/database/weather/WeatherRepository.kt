package com.eldorado.joshuasardinha.weatherforecastjoshua.database.weather

import android.app.Application
import android.os.AsyncTask
import com.eldorado.joshuasardinha.weatherforecastjoshua.database.RoomDB
import com.eldorado.joshuasardinha.weatherforecastjoshua.database.city.CityDao
import com.eldorado.joshuasardinha.weatherforecastjoshua.model.City
import com.eldorado.joshuasardinha.weatherforecastjoshua.model.CityWeather
import com.eldorado.joshuasardinha.weatherforecastjoshua.model.WeatherItem
import org.jetbrains.anko.doAsync

class WeatherRepository(application: Application) {
    private val db : RoomDB = RoomDB.getDatabase(application)!!
    private val mDao : WeatherDao = db.weatherDao()

    fun insert(weathers : List<CityWeather>) {
        doAsync {
            mDao.insert(weathers)
        }
    }

    fun getWeathersFromCity(cityName : String) : List<CityWeather> {
        return GetWeathersFromCityAsyncTask(mDao).execute(cityName).get()
    }

    fun deleteWeathersFromCity(cityName : String) {
        doAsync {
            mDao.deleteWeathersFromCity(cityName)
        }
    }

    fun updateWeathersFromCity(cityName : String, weathers: List<CityWeather>) {
        UpdateWeathersFromCityAsyncTask(mDao, weathers).execute(cityName)
    }

    private class UpdateWeathersFromCityAsyncTask(private val mAsyncTaskDao: WeatherDao,
                                                  private val weathers: List<CityWeather>) : AsyncTask<String, Void,  Void?>() {
        override fun doInBackground(vararg params: String):  Void? {
            mAsyncTaskDao.getWeathersFromCity(params[0])
            return null
        }

        override fun onPostExecute(params : Void?) {
            doAsync {
                mAsyncTaskDao.insert(weathers)
            }
        }
    }

    private class GetWeathersFromCityAsyncTask(private val mAsyncTaskDao: WeatherDao) : AsyncTask<String, Void,  List<CityWeather>>() {
        override fun doInBackground(vararg params: String):  List<CityWeather> {
            return mAsyncTaskDao.getWeathersFromCity(params[0])
        }

        override fun onPostExecute(result:  List<CityWeather>?) {
            super.onPostExecute(result)
        }
    }
}
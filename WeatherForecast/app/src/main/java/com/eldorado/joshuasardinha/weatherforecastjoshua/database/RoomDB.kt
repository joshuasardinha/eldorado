package com.eldorado.joshuasardinha.weatherforecastjoshua.database

import android.arch.persistence.db.SupportSQLiteDatabase
import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.TypeConverters
import android.content.Context
import com.eldorado.joshuasardinha.weatherforecastjoshua.database.city.CityDao
import com.eldorado.joshuasardinha.weatherforecastjoshua.database.weather.WeatherDao
import com.eldorado.joshuasardinha.weatherforecastjoshua.model.City
import com.eldorado.joshuasardinha.weatherforecastjoshua.model.CityWeather
import com.eldorado.joshuasardinha.weatherforecastjoshua.util.Converters

@Database(entities = [City::class, CityWeather::class], version = 1)  @TypeConverters(Converters::class)
abstract class RoomDB : RoomDatabase() {

    abstract fun cityDao() : CityDao
    abstract fun weatherDao() : WeatherDao

    companion object {
        private val sRoomDatabaseCallback = object : RoomDatabase.Callback() {
            override fun onOpen(db: SupportSQLiteDatabase) {
                super.onOpen(db)
            }
        }

        private var INSTANCE: RoomDB? = null

        fun getDatabase(context : Context): RoomDB? {
            if (INSTANCE == null) {
                synchronized(RoomDB::class) {
                    INSTANCE = Room.databaseBuilder(context,
                            RoomDB::class.java,"room_database")
                            .addCallback(sRoomDatabaseCallback)
                            .build()
                }
            }
            return INSTANCE
        }
    }

    fun destroyInstance() {
        INSTANCE = null
    }
}
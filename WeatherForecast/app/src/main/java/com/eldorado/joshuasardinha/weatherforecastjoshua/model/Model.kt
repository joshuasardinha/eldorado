package com.eldorado.joshuasardinha.weatherforecastjoshua.model

data class Model(val city: City,
                 val cod: Int,
                 val message: Float,
                 val cnt: Int,
                 val list: List<WeatherItem>)
package com.eldorado.joshuasardinha.weatherforecastjoshua.view.weather

import android.content.Context
import android.content.res.Resources
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import com.eldorado.joshuasardinha.weatherforecastjoshua.R
import com.eldorado.joshuasardinha.weatherforecastjoshua.model.CityWeather
import org.joda.time.DateTime
import org.joda.time.format.DateTimeFormat
import org.joda.time.format.DateTimeFormatter

class WeatherAdapter (private val mContext: Context, private var mWeathers : List<CityWeather>) : RecyclerView.Adapter<WeatherAdapter.ViewHolder>() {

    private val iconNamePrefix : String = "ic_weather_"
    private val mInflater: LayoutInflater = LayoutInflater.from(mContext)

    class ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView) {
        val weatherDate : TextView = itemView.findViewById(R.id.weatherItemDate)
        val weatherIcon : ImageView = itemView.findViewById(R.id.weatherItemIcon)
        val weatherTemp : TextView = itemView.findViewById(R.id.weatherItemTemp)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = mInflater.inflate(R.layout.item_weather, parent, false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (mWeathers != null) {
            val current : CityWeather = mWeathers!![position]

            val formatter : DateTimeFormatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss")
            val date : DateTime = formatter.parseDateTime(current.dt_txt)
            val weatherHour : String = date.hourOfDay().get().toString() + "h"
            holder.weatherDate.text = weatherHour

            val iconName : String = iconNamePrefix + current.icon
            val resources : Resources = mContext.resources
            val resourceId : Int = resources.getIdentifier(iconName,"drawable", mContext.packageName)
            holder.weatherIcon.setImageResource(resourceId)

            val tempText : String = ((current.temp_max + current.temp_min).toInt()/2 - 273).toString() + "°"
            holder.weatherTemp.text = tempText
        }
    }

    // getItemCount() is called many times, and when it is first called,
    // mTasks has not been updated (means initially, it's null, and we can't return null).
    override fun getItemCount(): Int {
        return if (mWeathers != null)
            mWeathers!!.size
        else
            0
    }
}

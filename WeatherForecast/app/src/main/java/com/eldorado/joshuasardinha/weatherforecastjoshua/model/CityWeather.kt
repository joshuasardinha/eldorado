package com.eldorado.joshuasardinha.weatherforecastjoshua.model

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.ForeignKey
import android.arch.persistence.room.ForeignKey.CASCADE
import android.arch.persistence.room.PrimaryKey
import android.support.annotation.NonNull

@Entity(tableName = "city_weather_table")
class CityWeather {
    @NonNull
    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    var id: Int = 0

    @ColumnInfo(name = "cityName")
    @ForeignKey(entity = City::class , parentColumns = ["id"], childColumns = ["cityName"], onDelete = CASCADE, onUpdate = CASCADE)
    var cityName: String = ""

    @ColumnInfo(name = "dt")
    var dt : Long = 0

    @ColumnInfo(name = "temp_min")
    var temp_min : Double = 0.0

    @ColumnInfo(name = "temp_max")
    var temp_max : Double = 0.0

    @ColumnInfo(name = "main")
    var main : String = ""

    @ColumnInfo(name = "description")
    var description : String = ""

    @ColumnInfo(name = "icon")
    var icon : String = ""

    @ColumnInfo(name = "dt_txt")
    var dt_txt : String = ""

}
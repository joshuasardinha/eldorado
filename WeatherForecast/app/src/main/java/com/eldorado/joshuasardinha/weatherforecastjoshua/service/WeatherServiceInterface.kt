package com.eldorado.joshuasardinha.weatherforecastjoshua.service

import com.eldorado.joshuasardinha.weatherforecastjoshua.model.Model
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query
import retrofit2.http.Url

interface WeatherServiceInterface {

    @GET("/data/2.5/forecast")
    fun getCityWeather(@Query("q") city: String,
                       @Query("appid") appid: String): Call<Model>
}
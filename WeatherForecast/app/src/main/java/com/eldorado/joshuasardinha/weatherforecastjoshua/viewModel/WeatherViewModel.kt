package com.eldorado.joshuasardinha.weatherforecastjoshua.viewModel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import com.eldorado.joshuasardinha.weatherforecastjoshua.database.weather.WeatherRepository
import com.eldorado.joshuasardinha.weatherforecastjoshua.model.City
import com.eldorado.joshuasardinha.weatherforecastjoshua.model.CityWeather
import com.eldorado.joshuasardinha.weatherforecastjoshua.model.Model
import com.eldorado.joshuasardinha.weatherforecastjoshua.model.WeatherItem

class WeatherViewModel(application: Application) : AndroidViewModel(application) {
    private val mRepository: WeatherRepository = WeatherRepository(application)

    fun updateWeathersFromCity(city : City, weather : Model?) {
        // Parse weathers data obtained to useful data to be stored
        val cityWeatherList = ArrayList<CityWeather>()
        for (weatherItem : WeatherItem in weather!!.list) {
            val cityWeather = CityWeather()
            cityWeather.cityName = city.name
            cityWeather.description = weatherItem.weather[0].description
            cityWeather.dt = weatherItem.dt
            cityWeather.temp_min = weatherItem.main.temp_min
            cityWeather.temp_max = weatherItem.main.temp_max
            cityWeather.main = weatherItem.weather[0].main
            cityWeather.icon = weatherItem.weather[0].icon
            cityWeather.dt_txt = weatherItem.dt_txt

            cityWeatherList.add(cityWeather)
        }

        // Update current list of weathers from this city
        mRepository.updateWeathersFromCity(city.name, cityWeatherList)
    }

    fun getWeathersFromCity(cityName: String) : List<CityWeather> {
        return mRepository.getWeathersFromCity(cityName)
    }
}
package com.eldorado.joshuasardinha.weatherforecastjoshua.service

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ServiceGenerator {

    companion object {
        private val gson : Gson = GsonBuilder().
                setDateFormat("yyyy-MM-dd HH:mm:ss").
                create()

        private val builder : Retrofit.Builder = Retrofit.Builder().
                baseUrl("http://api.openweathermap.org/").
                addConverterFactory(GsonConverterFactory.create(gson))

        private var retrofit : Retrofit = builder.build()

        private val loggingInterceptor : HttpLoggingInterceptor = HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY)

        private val httpClientBuilder : OkHttpClient.Builder = OkHttpClient.Builder()

        fun <S> createService(serviceClass : Class<S>) : S {
            if (!httpClientBuilder.interceptors().contains(loggingInterceptor)) {
                httpClientBuilder.addInterceptor(loggingInterceptor)
                val builder = builder.client(httpClientBuilder.build())
                retrofit = builder.build()
            }

            return retrofit.create(serviceClass)
        }
    }


}
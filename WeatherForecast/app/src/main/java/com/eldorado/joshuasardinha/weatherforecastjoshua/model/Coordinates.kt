package com.eldorado.joshuasardinha.weatherforecastjoshua.model

data class Coordinates(val lon: Double, val lat: Double)
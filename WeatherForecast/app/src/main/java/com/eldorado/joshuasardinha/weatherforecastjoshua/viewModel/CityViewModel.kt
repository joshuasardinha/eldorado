package com.eldorado.joshuasardinha.weatherforecastjoshua.viewModel

import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.arch.lifecycle.LiveData
import com.eldorado.joshuasardinha.weatherforecastjoshua.database.city.CityRepository
import com.eldorado.joshuasardinha.weatherforecastjoshua.model.City

class CityViewModel(application: Application): AndroidViewModel(application) {
    private val mRepository = CityRepository(application)
    private val mCities: LiveData<List<City>> = mRepository.getAllCities()

    fun insert(city: City) {
        mRepository.insert(city)
    }

    fun getAllCities() : LiveData<List<City>> {
        return mCities
    }

    fun delete(city: City) {
        mRepository.delete(city)
    }
}
package com.eldorado.joshuasardinha.weatherforecastjoshua.view.mainactivity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.constraint.ConstraintLayout
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import android.widget.Toast
import com.eldorado.joshuasardinha.weatherforecastjoshua.R
import com.eldorado.joshuasardinha.weatherforecastjoshua.view.weather.CityWeatherActivity
import com.eldorado.joshuasardinha.weatherforecastjoshua.model.City
import com.eldorado.joshuasardinha.weatherforecastjoshua.model.Model
import com.eldorado.joshuasardinha.weatherforecastjoshua.service.WeatherManager
import com.eldorado.joshuasardinha.weatherforecastjoshua.service.listener.WeatherServiceListener
import com.eldorado.joshuasardinha.weatherforecastjoshua.viewModel.WeatherViewModel
import org.joda.time.DateTime
import org.joda.time.Interval

class CityAdapter (context: Context, weatherViewModel: WeatherViewModel) : RecyclerView.Adapter<CityAdapter.ViewHolder>() {

    private val mContext = context
    private val weatherViewModel = weatherViewModel
    private val mInflater: LayoutInflater = LayoutInflater.from(context)
    private var mCities: List<City>? = null // Cached copy of words

    class ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView) {
        val cityBackground : ConstraintLayout = itemView.findViewById(R.id.cityBackground)
        val cityTextView : TextView = itemView.findViewById(R.id.cityName)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = mInflater.inflate(R.layout.city_item, parent, false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (mCities != null) {
            val current: City = mCities!![position]

            holder.cityTextView.text = current.name

            // Check if there is a difference of one day or more between the click moment and
            // the lastUpdateDate from this city. If there is, delete old weather data, download
            // and store new data and then start new activity
            holder.cityBackground.setOnClickListener { view ->
                val todayTime : DateTime = DateTime.now()
                val interval = Interval(DateTime(Long.MIN_VALUE),
                        todayTime.withTimeAtStartOfDay())
                val isAnotherDay : Boolean = interval.contains(current.lastUpdatedDate)

                if (isAnotherDay) {
                    val weatherManager = WeatherManager(mContext)

                    weatherManager.getCityWeather(object : WeatherServiceListener {
                        override fun onSuccess(weather: Model?, code: Int) {
                            if (code == 200) {
                                if (weather != null && weather.list.isNotEmpty()) {
                                    weatherViewModel.updateWeathersFromCity(current, weather)
                                    startCityWeatherActivity(current)
                                } else {
                                    Toast.makeText(mContext, "Server did not post weathers list, try again in some seconds.", Toast.LENGTH_LONG).show()
                                }
                            } else {
                                Toast.makeText(mContext,"Could not connect to server", Toast.LENGTH_SHORT).show()
                            }
                        }

                        override fun onFailure() {

                        }
                    }, current.name)
                } else {
                    startCityWeatherActivity(current)
                }
            }
        }
    }

    internal fun setCities(cities: List<City>?) {
        mCities = cities
        notifyDataSetChanged()
    }

    // getItemCount() is called many times, and when it is first called,
    // mTasks has not been updated (means initially, it's null, and we can't return null).
    override fun getItemCount(): Int {
        return if (mCities != null)
            mCities!!.size
        else
            0
    }

    fun getCityAtPosition(position : Int) : City {
        return mCities!![position]
    }

    private fun startCityWeatherActivity(city : City) {
        val intent = Intent(mContext, CityWeatherActivity::class.java)
        val bundle = Bundle()
        bundle.putString("CityName", city.name)
        intent.putExtras(bundle)
        mContext.startActivity(intent)

    }
}

package com.eldorado.joshuasardinha.weatherforecastjoshua.database.weather

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import com.eldorado.joshuasardinha.weatherforecastjoshua.model.CityWeather
import com.eldorado.joshuasardinha.weatherforecastjoshua.model.WeatherItem

@Dao
interface WeatherDao {
    @Insert
    fun insert(weathers: List<CityWeather>)

    @Query("SELECT * FROM city_weather_table WHERE cityName = :cityName")
    fun getWeathersFromCity(cityName : String) : List<CityWeather>

    @Query("DELETE FROM city_weather_table WHERE cityName = :cityName")
    fun deleteWeathersFromCity(cityName : String)
}
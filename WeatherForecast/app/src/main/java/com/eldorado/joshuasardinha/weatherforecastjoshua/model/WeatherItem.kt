package com.eldorado.joshuasardinha.weatherforecastjoshua.model

data class WeatherItem(val dt: Long,
                       val main: MainWeatherValues,
                       val weather: List<Weather>,
                       val clouds: Clouds,
                       val wind: Wind,
                       val sys: WeatherSys,
                       val dt_txt: String)
package com.motorola.moto4u.todolistjoshua.adapter

import android.content.Context
import com.motorola.moto4u.todolistjoshua.model.Task
import android.view.ViewGroup
import android.view.LayoutInflater
import android.support.v7.widget.RecyclerView
import android.view.View
import com.motorola.moto4u.todolistjoshua.R

import com.motorola.moto4u.todolistjoshua.viewmodel.TaskViewModel
import android.graphics.Color
import android.support.constraint.ConstraintLayout
import android.widget.*


class TaskListAdapter (context: Context, viewModel : TaskViewModel) : RecyclerView.Adapter<TaskListAdapter.ViewHolder>() {

    private var mTaskViewModel : TaskViewModel = viewModel
    private val mInflater: LayoutInflater  = LayoutInflater.from(context)
    private var mTasks: List<Task>? = null // Cached copy of words
    private var selectedPos = RecyclerView.NO_POSITION

    class ViewHolder(itemView : View) : RecyclerView.ViewHolder(itemView) {
        val taskBackground : ConstraintLayout = itemView.findViewById(R.id.background)
        val taskTextView : TextView = itemView.findViewById(R.id.textView)
        val taskCheckBox : CheckBox = itemView.findViewById(R.id.checkbox)
        val taskDelete : ImageView = itemView.findViewById(R.id.bin)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val itemView = mInflater.inflate(R.layout.recyclerview_item, parent, false)
        return ViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        if (mTasks != null) {
            if (position == selectedPos) {
                holder.taskBackground.setBackgroundColor(Color.parseColor("#CCCCCCCC"))
                holder.taskDelete.visibility = View.VISIBLE
            } else {
                holder.taskBackground.setBackgroundColor(Color.parseColor("#00FFFFFF"))
                holder.taskDelete.visibility = View.GONE
            }

            val current = mTasks!![position]
            holder.taskTextView.text = current.mTask
            holder.taskCheckBox.isChecked = current.completed
            holder.taskCheckBox.setOnCheckedChangeListener {_, isChecked ->
                holder.taskCheckBox.isChecked = isChecked
                current.completed = isChecked
                mTaskViewModel.update(current)
            }
            holder.taskBackground.setOnLongClickListener{
                if (selectedPos == position) {
                    selectedPos = RecyclerView.NO_POSITION
                    notifyDataSetChanged()
                }
                else {
                    notifyDataSetChanged()
                    selectedPos = position
                    notifyDataSetChanged()
                }
                true
            }
            holder.taskDelete.setOnClickListener {
                selectedPos = RecyclerView.NO_POSITION
                mTaskViewModel.deleteTask(current)
            }

        } else {
            // Covers the case of data not being ready yet.
            holder.taskTextView.text = "No Task"
            holder.taskCheckBox.isChecked = false
        }
    }

    internal fun setWords(tasks: List<Task>?) {
        mTasks = tasks
        notifyDataSetChanged()
    }

    // getItemCount() is called many times, and when it is first called,
    // mTasks has not been updated (means initially, it's null, and we can't return null).
    override fun getItemCount(): Int {
        return if (mTasks != null)
            mTasks!!.size
        else
            0
    }

    fun getTaskAtPosition(position : Int) : Task {
        return mTasks!![position]
    }
}

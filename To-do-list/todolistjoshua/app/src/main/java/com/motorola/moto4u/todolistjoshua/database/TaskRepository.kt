package com.motorola.moto4u.todolistjoshua.database

import android.app.Application
import android.arch.lifecycle.LiveData
import android.os.AsyncTask
import com.motorola.moto4u.todolistjoshua.model.Task

class TaskRepository(application : Application) {
    private val db : TaskRoomDatabase = TaskRoomDatabase.getDatabase(application)!!
    private var mTaskDao : TaskDao = db.taskDao()
    private var mAllTasks : LiveData<List<Task>> = mTaskDao.getAllTasks()

    fun getAllTasks() : LiveData<List<Task>> {
        return mAllTasks
    }

    fun getAllStoredTasks() : List<Task> {
        return GetAllTasksAsyncTask(mTaskDao).execute().get()
    }

    fun getAllTaskObjects() : List<Task> {
        return mTaskDao.getAllTaskObjects()
    }

    fun insert(task : Task) {
        InsertAsyncTask(mTaskDao).execute(task)
    }

    fun update(task : Task) {
        UpdateAsyncTask(mTaskDao).execute(task)
    }

    fun deleteTask(task : Task) {
        DeleteTaskAsyncTask(mTaskDao).execute(task)
    }

    private class InsertAsyncTask(private val mAsyncTaskDao: TaskDao) : AsyncTask<Task, Void, Void>() {
        override fun doInBackground(vararg params: Task): Void? {
            mAsyncTaskDao.insert(params[0])
            return null
        }
    }

    private class GetAllTasksAsyncTask(private val mAsyncTaskDao: TaskDao) : AsyncTask<Task, Void, List<Task>>() {
        override fun doInBackground(vararg params: Task?): List<Task> {
            return mAsyncTaskDao.getAllStoredTasks()
        }

        override fun onPostExecute(result: List<Task>?) {
            super.onPostExecute(result)
        }
    }

    private class UpdateAsyncTask(private val mAsyncTaskDao: TaskDao) : AsyncTask<Task, Void, Void>() {
        override fun doInBackground(vararg params: Task): Void? {
            mAsyncTaskDao.updateTask(params[0])
            return null
        }
    }

    private class DeleteTaskAsyncTask(private val mAsyncTaskDao : TaskDao) : AsyncTask<Task, Void, Void>() {
        override fun doInBackground(vararg params: Task): Void? {
            mAsyncTaskDao.deleteTask(params[0])
            return null
        }
    }
}
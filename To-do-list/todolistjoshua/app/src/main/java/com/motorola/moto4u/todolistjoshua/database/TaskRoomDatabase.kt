package com.motorola.moto4u.todolistjoshua.database

import android.arch.persistence.db.SupportSQLiteDatabase
import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import android.os.AsyncTask
import com.motorola.moto4u.todolistjoshua.model.Task

@Database(entities = [Task::class], version =  2)
abstract class TaskRoomDatabase : RoomDatabase() {

    abstract fun taskDao(): TaskDao

    companion object {
        private val sRoomDatabaseCallback = object : RoomDatabase.Callback() {
            override fun onOpen(db: SupportSQLiteDatabase) {
                super.onOpen(db)
                PopulateDbAsync(INSTANCE).execute()
            }
        }

        private var INSTANCE: TaskRoomDatabase? = null

        fun getDatabase(context : Context): TaskRoomDatabase? {
            if (INSTANCE == null) {
                synchronized(TaskRoomDatabase::class) {
                    INSTANCE = Room.databaseBuilder(context,
                            TaskRoomDatabase::class.java,"task_database")
                            .addCallback(sRoomDatabaseCallback)
                            .build()
                }
            }
            return INSTANCE
        }
    }

    fun destroyInstance() {
        INSTANCE = null
    }


}

private class PopulateDbAsync internal constructor(db: TaskRoomDatabase?) : AsyncTask<Void, Void, Void>() {

    private val mDao: TaskDao = db!!.taskDao()

    override fun doInBackground(vararg params: Void): Void? {
        /*mDao.deleteAll()
        var word = Task("Hello", false)
        mDao.insert(word)
        word = Task("World", false)
        mDao.insert(word)*/
        return null
    }
}

package com.motorola.moto4u.todolistjoshua.viewmodel

import com.motorola.moto4u.todolistjoshua.model.Task
import android.arch.lifecycle.LiveData
import com.motorola.moto4u.todolistjoshua.database.TaskRepository
import android.app.Application
import android.arch.lifecycle.AndroidViewModel
import android.databinding.Bindable
import com.motorola.moto4u.todolistjoshua.adapter.TaskListAdapter

class TaskViewModel(application: Application) : AndroidViewModel(application) {

    private val mRepository: TaskRepository = TaskRepository(application)
    private val allTasks: LiveData<List<Task>> = mRepository.getAllTasks()

    fun insert(task: Task) {
        mRepository.insert(task)
    }

    fun getAllWords(): LiveData<List<Task>> {
        return allTasks
    }

    fun update(task : Task) {
        mRepository.update(task)
    }

    fun deleteTask(task : Task) {
        mRepository.deleteTask(task)
    }

}
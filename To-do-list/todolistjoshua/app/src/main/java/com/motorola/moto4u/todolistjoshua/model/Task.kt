package com.motorola.moto4u.todolistjoshua.model

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.support.annotation.*

@Entity(tableName = "task_table")
data class Task(
        @NonNull @PrimaryKey @ColumnInfo(name = "task") val mTask : String,
        @ColumnInfo(name = "completed") var completed : Boolean)
package com.motorola.moto4u.todolistjoshua.activity

import android.app.Activity
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.Menu
import android.view.MenuItem
import com.motorola.moto4u.todolistjoshua.R
import com.motorola.moto4u.todolistjoshua.adapter.TaskListAdapter
import com.motorola.moto4u.todolistjoshua.model.Task
import com.motorola.moto4u.todolistjoshua.viewmodel.TaskViewModel

import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*
import android.widget.Toast
import android.content.Intent
import android.support.v7.widget.helper.ItemTouchHelper

class MainActivity : AppCompatActivity() {

    val EXTRA_REPLY = "com.example.android.wordlistsql.REPLY"
    val NEW_WORD_ACTIVITY_REQUEST_CODE = 1

    private lateinit var mTaskViewModel : TaskViewModel
    private val mContext : Context = this

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)

        mTaskViewModel = ViewModelProviders.of(this).get(TaskViewModel::class.java)

        val recyclerView : RecyclerView = recyclerview
        val adapter = TaskListAdapter(this, mTaskViewModel)
        recyclerView.apply {
            this.adapter = adapter
            this.layoutManager = LinearLayoutManager(mContext)
        }

        val helper : ItemTouchHelper = ItemTouchHelper(object : ItemTouchHelper.SimpleCallback(0,
                ItemTouchHelper.LEFT or ItemTouchHelper.RIGHT) {
            override fun onMove(recyclerView: RecyclerView,
                                viewHolder: RecyclerView.ViewHolder,
                                target: RecyclerView.ViewHolder): Boolean {
                return false
            }

            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val position = viewHolder.adapterPosition
                val myTask = adapter.getTaskAtPosition(position)
                Toast.makeText(this@MainActivity, "Deleting " + myTask.mTask, Toast.LENGTH_LONG).show()

                mTaskViewModel.deleteTask(myTask)
            }
        } )

        helper.attachToRecyclerView(recyclerView)


        mTaskViewModel.getAllWords().observe(this, Observer { words ->
            // Update the cached copy of the words in the adapter.
            adapter.setWords(words)
        })

        fab.setOnClickListener { _ ->
            val intent = Intent(this@MainActivity, NewTaskActivity::class.java)
            startActivityForResult(intent, NEW_WORD_ACTIVITY_REQUEST_CODE)
        }

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }

    public override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == NEW_WORD_ACTIVITY_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            val task = Task(data!!.getStringExtra(EXTRA_REPLY), false)
            mTaskViewModel.insert(task)
            val liveData = mTaskViewModel.getAllWords()
            val i = 2
        } else {
            Toast.makeText(
                    applicationContext,
                    R.string.empty_not_saved,
                    Toast.LENGTH_LONG).show()
        }
    }

}

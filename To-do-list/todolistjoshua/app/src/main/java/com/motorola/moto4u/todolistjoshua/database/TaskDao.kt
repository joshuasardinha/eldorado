package com.motorola.moto4u.todolistjoshua.database

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.*
import com.motorola.moto4u.todolistjoshua.model.Task

@Dao
interface TaskDao {
    @Insert
    fun insert(task : Task)

    @Query("DELETE FROM task_table")
    fun deleteAll()

    @Query("SELECT * FROM task_table ORDER BY task ASC")
    fun getAllTasks() : LiveData<List<Task>>

    @Query("SELECT * FROM task_table ORDER BY task ASC")
    fun getAllStoredTasks() : List<Task>

    @Query("SELECT * FROM task_table ORDER BY task ASC")
    fun getAllTaskObjects() : List<Task>

    @Update
    fun updateTask(vararg task : Task)

    @Delete
    fun deleteTask(task : Task)

}
package com.motorola.moto4u.eldoradotraining.model

class Contact(name: String) {
    var contactName = name

    fun getName() : String {
        return contactName
    }

    fun setName(name : String) {
        contactName = name
    }
}